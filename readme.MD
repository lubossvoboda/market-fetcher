https://medium.com/arhs-spikeseed/how-to-deploy-and-scale-your-app-in-minutes-with-containers-and-aws-ecs-e49dae446928
https://hackernoon.com/running-docker-on-aws-ec2-83a14b780c56

* Docker

1. sudo docker build -f Dockerfile -t market-fetcher .

2. sudo docker run -p 8080:8080 market-fetcher

3. sudo docker ps -a

secretKey

* AWS EC2
1. chmod 400 master-key-pair.pem
2. ssh -i master-key-pair.pem ec2-user@ec2-54-194-37-94.eu-west-1.compute.amazonaws.com

3. aws configure

4. aws ecr get-login --no-include-email --region eu-west-1
+ login to ecr
5.  sudo docker tag market-fetcher:latest 317443945083.dkr.ecr.eu-west-1.amazonaws.com/market-fetcher:latest

6. sudo docker push 317443945083.dkr.ecr.eu-west-1.amazonaws.com/market-fetcher:latest

7. Login to EC2 instance

8. sudo docker pull 317443945083.dkr.ecr.eu-west-1.amazonaws.com/market-fetcher

9. sudo docker run -p 8080:8080 317443945083.dkr.ecr.eu-west-1.amazonaws.com/market-fetcher

10. sudo docker exec -it 787dd1509546 /bin/sh