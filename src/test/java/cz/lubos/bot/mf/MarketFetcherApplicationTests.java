package cz.lubos.bot.mf;

import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MarketFetcherApplicationTests {

	@Autowired
	Environment env;

	@Test
	public void contextLoads() {
		System.out.println(Arrays.toString((env.getActiveProfiles())));
	}

}
