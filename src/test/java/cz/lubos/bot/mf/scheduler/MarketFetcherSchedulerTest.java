package cz.lubos.bot.mf.scheduler;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.lubos.bot.mf.api.BittrexMarketSummary;
import cz.lubos.bot.mf.data.DataService;
import cz.lubos.bot.mf.trade.BittrexService;
import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class MarketFetcherSchedulerTest {

    @Mock
    BittrexService botTradeService;

    @Mock
    DataService dataService;


    @InjectMocks
    MarketFetcherScheduler scheduler;

    @Test
    @Ignore
    public void shouldSendUpdateToBotWhenMarketDataFetched() {
        List<BittrexMarketSummary> bittrexMarketSummaryList = new ArrayList<>();
        when(botTradeService.getBittrexMarketSummary()).thenReturn(bittrexMarketSummaryList);

        scheduler.triggerBittrexMarketSummarySnapshot();

        ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);

        assertEquals(argument.getValue(), bittrexMarketSummaryList);
    }

    @Test
    @Ignore
    public void shouldSaveDataToStorageWhenMarketDataFetched() {
        List<BittrexMarketSummary> bittrexMarketSummaryList = new ArrayList<>();
        when(botTradeService.getBittrexMarketSummary()).thenReturn(bittrexMarketSummaryList);

        scheduler.triggerBittrexMarketSummarySnapshot();

        ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
        verify(dataService).saveBittrex(argument.capture());

        assertEquals(argument.getValue(), bittrexMarketSummaryList);
    }


    @Test
    public void shouldReportToAuditWhenExceptionOccurred() {
        when(botTradeService.getBittrexMarketSummary()).thenThrow(new RuntimeException());

        scheduler.triggerBittrexMarketSummarySnapshot();
    }
}
