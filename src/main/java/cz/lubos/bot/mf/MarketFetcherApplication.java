package cz.lubos.bot.mf;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@Slf4j
public class MarketFetcherApplication {

    public static void main(String[] args) {
        log.info("Starting Market Fetcher");
        SpringApplication.run(MarketFetcherApplication.class, args);
    }
}
