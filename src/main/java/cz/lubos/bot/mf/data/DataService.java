package cz.lubos.bot.mf.data;

import com.binance.api.client.domain.market.TickerPrice;
import cz.lubos.bot.mf.api.BittrexMarketSummary;
import java.util.List;

public interface DataService {

    /***
     * Store list of market summary objects to external storage
     * @param bittrexMarketSummary
     */
    void saveBittrex(List<BittrexMarketSummary> bittrexMarketSummary);

    void saveBinance(List<TickerPrice> allPrices);
}
