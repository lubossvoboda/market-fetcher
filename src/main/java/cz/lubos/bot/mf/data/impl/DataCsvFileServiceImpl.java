package cz.lubos.bot.mf.data.impl;

import com.binance.api.client.domain.market.TickerPrice;
import cz.lubos.bot.mf.api.BittrexMarketSummary;
import cz.lubos.bot.mf.config.properties.AppConfigProperties;
import cz.lubos.bot.mf.data.DataService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class DataCsvFileServiceImpl extends DataCsvServiceImpl implements DataService {

    private final AppConfigProperties appConfigProperties;

    @Override
    public void saveBittrex(List<BittrexMarketSummary> bittrexMarketSummary) {

    }

    @Override
    public void saveBinance(List<TickerPrice> allPrices) {

    }
/*
    @Override
    public void saveBittrex(MarketSummary marketSummary) {
        try {
            String csvLine = convertToCSVLine(marketSummary);

            LocalDateTime date = LocalDateTime.parse(marketSummary.getTimeStamp());
            String dateSuffix = date.format(BASIC_ISO_DATE);

            String marketName = marketSummary.getMarketName();

            Path logPath = Paths.get(appConfigProperties.getDataDir() + generateFileName(marketName, dateSuffix));
            Files.write(logPath, csvLine.toString().getBytes(), APPEND, CREATE);
        } catch (Exception e) {
            throw new DataException(e.getMessage(), e);
        }
    }

    @Override
    public void saveBittrex(List<MarketSummary> marketSummary) {
        if (marketSummary.size() == 0) {
            return;
        }
        String content = marketSummary.stream()
                .map(ms -> convertToCSVLine(ms))
                .collect(Collectors.joining(System.lineSeparator()));

        try {
            LocalDateTime date = LocalDateTime.parse(marketSummary.get(0).getTimeStamp());
            String dateSuffix = date.format(BASIC_ISO_DATE);

            String marketName = marketSummary.getMarketName();

            Path logPath = Paths.get(appConfigProperties.getDataDir() + generateFileName(marketName, dateSuffix));
            Files.write(logPath, csvLine.toString().getBytes(), APPEND, CREATE);
        } catch (Exception e) {
            throw new DataException(e.getMessage(), e);
        }
    }

    @Override
    protected String convertToCSVLine(MarketSummary marketSummary) {
        StringJoiner line = new StringJoiner(CSV_DELIMITER);
        line.add(marketSummary.getMarketName());
        line.add(marketSummary.getTimeStamp());
        line.add(String.valueOf(marketSummary.getLast()));
        line.add(String.valueOf(marketSummary.getBaseVolume()));
        line.add(String.valueOf(marketSummary.getOpenBuyOrders()));
        line.add(String.valueOf(marketSummary.getOpenSellOrders()));
        return line.toString();
    }
    */
}
