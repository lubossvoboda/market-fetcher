package cz.lubos.bot.mf.data.impl;

import cz.lubos.bot.mf.api.BittrexMarketSummary;
import java.util.StringJoiner;

public class DataCsvServiceImpl {

    protected static final String CSV_DELIMITER = ",";
    private static final String FILE_NAME_PATTERN = "%s_%s.csv";

    protected String convertToCSVLine(BittrexMarketSummary bittrexMarketSummary) {
        StringJoiner line = new StringJoiner(CSV_DELIMITER);
        line.add(bittrexMarketSummary.getMarketName());
        line.add(bittrexMarketSummary.getTimeStamp());
        line.add(String.valueOf(bittrexMarketSummary.getLast()));
        line.add(String.valueOf(bittrexMarketSummary.getBaseVolume()));
        line.add(String.valueOf(bittrexMarketSummary.getOpenBuyOrders()));
        line.add(String.valueOf(bittrexMarketSummary.getOpenSellOrders()));
        return line.toString();
    }

    protected String generateFileName(String marketName, String dateSuffix) {
        return String.format(FILE_NAME_PATTERN, marketName, dateSuffix);
    }

}
