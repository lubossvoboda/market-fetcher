package cz.lubos.bot.mf.data.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.binance.api.client.domain.market.TickerPrice;
import cz.lubos.bot.mf.api.BittrexMarketSummary;
import cz.lubos.bot.mf.config.properties.AppConfigProperties;
import cz.lubos.bot.mf.data.DataException;
import cz.lubos.bot.mf.data.DataService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
@Primary
public class DataCsvAmazonS3ServiceImpl extends DataCsvServiceImpl implements DataService {

    @Value("${aws.s3.bucket-name.bittrex:}")
    private String bucketNameBittrex;

    @Value("${aws.s3.bucket-name.binance:}")
    private String bucketNameBinance;

    private final AppConfigProperties appConfigProperties;
    private final AmazonS3 amazonS3;


    private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");

    private static final String DELIMITER = ",";
    private static final String FILE_NAME_PATTERN = "%s%s_%s.csv";


    @Override
    public void saveBittrex(List<BittrexMarketSummary> bittrexMarketSummary) {

        if (bittrexMarketSummary.size() == 0) {
            return;
        }
        log.debug("Saving summaries to S3....");

        String content = bittrexMarketSummary.stream()
                .map(ms -> convertToCSVLine(ms))
                .collect(Collectors.joining(System.lineSeparator()));

        try {
            LocalDateTime date = LocalDateTime.now();
            String datePrefix = date.format(dateTimeFormatter);

            String fileName = LocalDate.now() + "/" + datePrefix + ".csv";

            log.debug("File: " + fileName);
            amazonS3.putObject(bucketNameBittrex, fileName, content);
        } catch (Exception e) {
            throw new DataException(e.getMessage(), e);
        }

        log.debug("...DONE");
    }

    @Override
    public void saveBinance(List<TickerPrice> allPrices) {
        if (allPrices.size() == 0) {
            return;
        }
        log.debug("Saving summaries to S3....");

        LocalDateTime now = LocalDateTime.now();
        String content = allPrices.stream()
                .map(ms -> convertToCSVLine(now, ms))
                .collect(Collectors.joining(System.lineSeparator()));

        try {
            String datePrefix = now.format(dateTimeFormatter);

            String fileName = LocalDate.now() + "/" + datePrefix + ".csv";

            amazonS3.putObject(bucketNameBinance, fileName, content);
        } catch (Exception e) {
            throw new DataException(e.getMessage(), e);
        }

        log.debug("...DONE");
    }

    private String convertToCSVLine(LocalDateTime now, TickerPrice ms) {
        StringJoiner line = new StringJoiner(CSV_DELIMITER);
        line.add(now.toString());
        line.add(ms.getSymbol());
        line.add(ms.getPrice());
        return line.toString();
    }
}
