package cz.lubos.bot.mf.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="trade.bittrex")
@Component
@Data
public class BittrexConfigProperties {

    private String username;
    private String apiKey;
    private String secretKey;
}
