package cz.lubos.bot.mf.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix="market.fetcher")
@Component
@Data
public class AppConfigProperties {

    private String dataDir;

    private String btcBotUrl;

    private Boolean btcBotSendEnabled;
}
