package cz.lubos.bot.mf.trade;

import com.binance.api.client.domain.market.TickerPrice;
import java.util.List;

public interface BinanceService {

    List<TickerPrice> getAllPrices();
}
