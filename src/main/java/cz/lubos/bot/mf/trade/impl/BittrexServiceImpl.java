package cz.lubos.bot.mf.trade.impl;


import cz.lubos.bot.mf.api.BittrexMarketSummary;
import cz.lubos.bot.mf.config.properties.BittrexConfigProperties;
import cz.lubos.bot.mf.trade.BittrexService;
import cz.lubos.bot.mf.trade.TradeMarketException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.bittrex.service.BittrexMarketDataServiceRaw;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class BittrexServiceImpl implements BittrexService {

    private final BittrexConfigProperties bittrexConfigProperties;

    protected BittrexMarketDataServiceRaw marketDataService;

    @PostConstruct
    public void init() {
        ExchangeSpecification exSpec = new BittrexExchange().getDefaultExchangeSpecification();
        exSpec.setUserName(bittrexConfigProperties.getUsername());
        exSpec.setApiKey(bittrexConfigProperties.getApiKey());
        exSpec.setSecretKey(bittrexConfigProperties.getSecretKey());
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(exSpec);
        marketDataService = (BittrexMarketDataServiceRaw) exchange.getMarketDataService();
    }


    @Override
    public List<BittrexMarketSummary> getBittrexMarketSummary() {
        try {
            List<org.knowm.xchange.bittrex.dto.marketdata.BittrexMarketSummary> list = marketDataService.getBittrexMarketSummaries();
            List<BittrexMarketSummary> result = new ArrayList<>(list.size());

            // decouple external lib dependency
            list.forEach(bms -> {
                BittrexMarketSummary ms = new BittrexMarketSummary();
                BeanUtils.copyProperties(bms, ms);
                result.add(ms);
            });
            return result;
        } catch (Exception e) {
            throw new TradeMarketException(e.getMessage(), e);
        }
    }
}
