package cz.lubos.bot.mf.trade;

import cz.lubos.bot.mf.api.BittrexMarketSummary;
import java.util.List;

public interface BittrexService {

    /**
     * Read latest data from crypto exchange
     * @return list of market summary object for each trading pair
     */
    List<BittrexMarketSummary> getBittrexMarketSummary();
}
