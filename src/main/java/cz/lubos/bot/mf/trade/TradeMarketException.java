package cz.lubos.bot.mf.trade;

public class TradeMarketException extends RuntimeException {

    public TradeMarketException(String message, Throwable cause) {
        super(message, cause);
    }
}
