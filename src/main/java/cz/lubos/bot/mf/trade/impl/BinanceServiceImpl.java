package cz.lubos.bot.mf.trade.impl;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.TickerPrice;
import cz.lubos.bot.mf.trade.BinanceService;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class BinanceServiceImpl implements BinanceService {

    private BinanceApiRestClient client;

    @PostConstruct
    public void init() {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance();
        client = factory.newRestClient();
    }

    @Override
    public List<TickerPrice> getAllPrices() {
        return client.getAllPrices();
    }
}
