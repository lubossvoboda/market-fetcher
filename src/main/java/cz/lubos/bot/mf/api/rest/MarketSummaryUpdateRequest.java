package cz.lubos.bot.mf.api.rest;

import cz.lubos.bot.mf.api.BittrexMarketSummary;
import java.util.List;
import lombok.Data;

@Data
public class MarketSummaryUpdateRequest {

    private List<BittrexMarketSummary> data;

}
