package cz.lubos.bot.mf.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Currency {
    private final String code;

    @Override
    public String toString() {
        return code;
    }
}
