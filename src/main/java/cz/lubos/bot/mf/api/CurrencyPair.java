package cz.lubos.bot.mf.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CurrencyPair {

    public final Currency base;
    public final Currency counter;

    /**
     * <p>
     * String constructor
     * </p>
     * In general the CurrencyPair.base is what you're wanting to buy/sell. The CurrencyPair.counter is what currency you want to use to pay/receive for
     * your purchase/sale.
     *
     * @param baseSymbol The base symbol is what you're wanting to buy/sell
     * @param counterSymbol The counter symbol is what currency you want to use to pay/receive for your purchase/sale.
     */
    public CurrencyPair(String baseSymbol, String counterSymbol) {
        this(new Currency(baseSymbol), new Currency(counterSymbol));
    }

    /**
     * Parse currency pair from a string in the same format as returned by toString() method - ABC/XYZ
     */
    public CurrencyPair(String currencyPair) {

        int split = currencyPair.indexOf('/');
        if (split < 1) {
            throw new IllegalArgumentException("Could not parse currency pair from '" + currencyPair + "'");
        }
        String base = currencyPair.substring(0, split);
        String counter = currencyPair.substring(split + 1);

        this.base = new Currency(base);
        this.counter = new Currency(counter);
    }

    @Override
    public String toString() {
        return toPairString(base.toString(),  counter.toString());
    }

    public static String toPairString(String base, String counter) {
        return base + "/" + counter;
    }
}
