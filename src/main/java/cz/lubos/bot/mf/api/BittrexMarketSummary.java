package cz.lubos.bot.mf.api;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BittrexMarketSummary {

    private BigDecimal ask;
    private BigDecimal bid;
    private BigDecimal high;
    private BigDecimal last;
    private BigDecimal low;
    private String marketName;
    private String timeStamp;
    private BigDecimal baseVolume;
    private BigDecimal volume;
    private int openBuyOrders;
    private int openSellOrders;

    public BittrexMarketSummary(BigDecimal last, String marketName) {
        this.last = last;
        this.marketName = marketName;
    }

    public CurrencyPair getCurrencyPair() {
        String[] pair = marketName.split("-");
        return new CurrencyPair(pair[1], pair[0]);
    }

    public LocalDateTime getLocalDateTimeStamp() {
        return LocalDateTime.parse(timeStamp);
    }
}
