package cz.lubos.bot.mf.scheduler;

import com.binance.api.client.domain.market.TickerPrice;
import cz.lubos.bot.mf.api.BittrexMarketSummary;
import cz.lubos.bot.mf.data.DataService;
import cz.lubos.bot.mf.trade.BinanceService;
import cz.lubos.bot.mf.trade.BittrexService;
import java.time.LocalDateTime;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class MarketFetcherScheduler {

    private final BittrexService botTradeService;
    private final BinanceService binanceService;

    private final DataService dataService;

    @Scheduled(fixedRate = 60000)
    public void triggerBittrexMarketSummarySnapshot() {
        try {
            log.debug("------------- Market summary update START -------------" + LocalDateTime.now());
            long start = System.currentTimeMillis();

            List<BittrexMarketSummary> summary = botTradeService.getBittrexMarketSummary();
            log.debug("...update received: " + summary.size());

            dataService.saveBittrex(summary);

            long end = System.currentTimeMillis();
            log.debug("Triggered Market summary snapshot in " + (end - start) + " [ms], " + summary.size() + " markets updated");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.debug("------------- Market summary update END -------------" + LocalDateTime.now());
    }


    @Scheduled(fixedRate = 60000)
    public void triggerBinanceAllPrice() {
        try {
            log.debug("------------- Binance all price update START -------------" + LocalDateTime.now());
            long start = System.currentTimeMillis();

            List<TickerPrice> allPrices = binanceService.getAllPrices();
            log.debug("...update received: " + allPrices.size());

            dataService.saveBinance(allPrices);

            long end = System.currentTimeMillis();
            log.debug("Triggered Binance all price snapshot in " + (end - start) + " [ms], " + allPrices.size() + " markets updated");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.debug("------------- Binance all price update END -------------" + LocalDateTime.now());
    }


    @Scheduled(cron = "0 30 7 * * *")
    public void triggerSendAliveStatus() {

    }
}
