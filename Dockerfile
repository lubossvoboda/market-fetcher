FROM openjdk:8-jdk-alpine

EXPOSE 8080

CMD ["java", "-jar", "/usr/share/market-fetcher-0.0.2-SNAPSHOT.jar"]

# Add the service itself
ARG JAR_FILE
ADD build/libs/market-fetcher-0.0.2-SNAPSHOT.jar /usr/share/market-fetcher-0.0.2-SNAPSHOT.jar